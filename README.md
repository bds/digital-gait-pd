# Digital Gait Sensor Data Analysis for Parkinson's Disease

# Table of contents
* [Introduction](#introduction)
* [Content](#content)
* [Data](#data)
* [Requirements](#requirements)
* [License](#license)
* [Instructions](#instructions)

# Introduction
This repository contains the code for statistical analyses described in the article "Integrating digital gait sensor data with metabolomics and clinical data to predict clinically relevant outcomes in Parkinson’s disease".

This project studied the use of digital gait sensor data, integrated with metabolomics and clinical data, to predict clinically relevant outcomes in Parkinson’s disease, assisting in diagnosis, severity assessment, prediction of gait impairments and comorbidities.

# Content
The code covers the following main analysis steps:

1. Loading, preparation and processing of datasets
2. Time series feature extraction
3. Machine learning modeling
4. Cross-validation analyses
5. Model interpretation (SHAP value analysis)


# Data
The personal clinical, gait, and metabolomics data used for this manuscript is not publicly available due to compliance with strict personal data protection regulations. Requests for access to the dataset should be directed to request.ncer-pd@uni.lu, and will be considered in accordance with these regulations.

# Requirements
The code was written entirely in R. It relies on multiple R and BioConductor packages, listed at the beginning of the corresponding R scripts.

# License
The code is available under the MIT License.

# Instructions
The code was tested to run in R4.2.0 on both current Mac (Ventura) and Linux operating systems (Ubuntu 23.04), but should be compatible with later versions of R installed on current Mac, Linux or Windows systems.
R software packages loaded at the beginning of the R scripts must be installed before using the code. R packages available on CRAN can be installed with the command:

install.packages("BiocManager::install("ADD_NAME_OF_THE_PACKAGE")

R packages from Bioconductor can be installed with the following commands:

if (!require("BiocManager", quietly = TRUE))
    install.packages("BiocManager")

BiocManager::install("ADD_NAME_OF_THE_PACKAGE")

To run the code, the correct working directory containing the input data must be specified at the beginning of the R-scripts, otherwise the scripts can be run as-is.